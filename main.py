from neuralNetwork import NeuralNetwork
import numpy
import matplotlib.pyplot as plt
import imageio

inodes = 784
hnodes = 100
onodes = 10
lrate = 0.2
config_file = "configs1.npz"

nn = NeuralNetwork(inodes, hnodes, onodes, lrate, config_file)


def train():
    # read the data from the mnist file
    data_file = open("mnist_train.csv", 'r')
    data_list = data_file.readlines()
    data_file.close()

    epochs = 5
    for e in range(epochs):
        for a in data_list:
            all_values = a.split(',')
            scaled_inputs = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
            targets = numpy.zeros(onodes) + 0.01
            targets[int(all_values[0])] = 0.99
            nn.train(scaled_inputs, targets)

    nn.save()


def test():
    test_data_file = open("mnist_test.csv")
    test_data_list = test_data_file.readlines()
    test_data_file.close()

    scorecard = []

    for recs in test_data_list:
        all_values = recs.split(',')
        correct_label = int(all_values[0])
        # print(correct_label, "correct label")
        inputs = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
        outs = nn.query(inputs)
        answ = int(numpy.argmax(outs))
        chance = (outs[answ] / outs.sum()) * 100
        # print(answ, "---", chance)

        if answ == correct_label:
            scorecard.append(1)
        else:
            scorecard.append(0)

    print("performance:", numpy.asarray(scorecard).sum() / numpy.asarray(scorecard).size)
    # plt.imshow(numpy.asfarray(test_data_list[9].split(',')[1:]).reshape((28, 28)), cmap='Greys', interpolation='None')
    # plt.show()


def query_my_image(img):
    img_array = imageio.imread(img, pilmode='P')
    img_data = 255.0 - numpy.reshape(img_array, (1, 784))
    img_data = (img_data / 255.0 * 0.99) + 0.01
    outputs = nn.query(img_data)
    answer = int(numpy.argmax(outputs))
    chancee = (outputs[answer] / outputs.sum()) * 100
    print(answer, "---", chancee)
    plt.imshow(img_data.reshape(28, 28), cmap='Greys', interpolation='None')
    plt.show()


nn.load()
query_my_image("handschrift_test.png")
