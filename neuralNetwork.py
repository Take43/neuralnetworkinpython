import csv

import numpy
import scipy.special


class NeuralNetwork:

    # initialise the neural Network
    def __init__(self, inputnodes, hiddennodes, outputnodes, learningrate, config_file):
        # specifying the shape of the neural Network
        self.inodes = inputnodes
        self.hnodes = hiddennodes
        self.onodes = outputnodes
        self.lrate = learningrate
        self.cfile = config_file

        # initialize the weights as a matrix
        # weight-matrix for the weights between the input and hidden layer
        self.wih = numpy.random.normal(0.0, pow(self.inodes, -0.5), (self.hnodes, self.inodes))
        # weight-matrix for the weights between the hidden and output layer
        self.who = numpy.random.normal(0.0, pow(self.hnodes, -0.5), (self.onodes, self.hnodes))

        # specify the activation function which emulates the firing neuron -> sigmoid-function
        self.activation_function = lambda x: scipy.special.expit(x)

    # train the neural Network
    def train(self, inputs_list, targets_list):
        # converting the lists of the ins and targets into matrices
        inputs = numpy.array(inputs_list, ndmin=2).T
        targets = numpy.array(targets_list, ndmin=2).T

        # calculate the input to the nodes of the hidden layer -> multiply weight-matrix for inputs with input-matrix
        hidden_inputs = numpy.dot(self.wih, inputs)
        # calculate the output of the nodes of the hidden layer -> activation function
        hidden_outputs = self.activation_function(hidden_inputs)

        # calculate the input into the output-layer -> multiply weight-matrix for hidden-outputs with hidden-outputs
        final_inputs = numpy.dot(self.who, hidden_outputs)
        # calculate the output of the nodes of the output-layer -> activation function
        final_outputs = self.activation_function(final_inputs)

        # calculating the error -> error = target - final_outs
        out_errors = targets - final_outputs

        # calculating the error of the hidden-layer -> output-errors split by weights recombined at hidden-nodes
        hidden_errs = numpy.dot(self.who.T, out_errors)

        # updating the weights between the hidden and output-layer
        self.who += self.lrate * numpy.dot((out_errors * final_outputs * (1.0 - final_outputs)), numpy.transpose(hidden_outputs))
        # updating the weights between the input and hidden-layer
        self.wih += self.lrate * numpy.dot((hidden_errs * hidden_outputs * (1.0 - hidden_outputs)), numpy.transpose(inputs))

        pass

    # query the neural Network
    def query(self, inputs_list):
        # converting the list of inputs into a matrix
        inputs = numpy.array(inputs_list, ndmin=2).T

        # calculate the input to the nodes of the hidden layer -> multiply weight-matrix for inputs with input-matrix
        hidden_input = numpy.dot(self.wih, inputs)
        # calculate the output of the nodes of the hidden layer -> activation function
        hidden_outputs = self.activation_function(hidden_input)

        # calculate the input into the output-layer -> multiply weight-matrix for hidden-outputs with hidden-outputs
        output_input = numpy.dot(self.who, hidden_outputs)
        # calculate the output of the nodes of the output-layer -> activation function
        output_out = self.activation_function(output_input)

        return output_out

    def save(self):
        numpy.savez(self.cfile, self.wih, self.who)

    def load(self):
        npzfile = numpy.load(self.cfile)
        self.wih = npzfile['arr_0']
        self.who = npzfile['arr_1']
